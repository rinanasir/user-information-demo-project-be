package com.mySoft.userinformationdemoprojectbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserInformationDemoProjectBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserInformationDemoProjectBeApplication.class, args);
	}

}
