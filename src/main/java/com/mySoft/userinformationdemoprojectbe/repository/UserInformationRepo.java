package com.mySoft.userinformationdemoprojectbe.repository;

import com.mySoft.userinformationdemoprojectbe.entity.UserInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInformationRepo extends JpaRepository<UserInformation, Long> {

}
